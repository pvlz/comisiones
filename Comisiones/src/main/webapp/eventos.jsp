<%@page import="com.google.appengine.api.datastore.QueryResultIterable"%>
<%@page import="com.google.appengine.api.datastore.QueryResultIterator"%>
<%@page import="comision.Evento"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Eventos</title>
</head>
<body>

	 <h1>Comisiones</h1>
	 
	 <table style="width:80%">
	 <thead>
	  <tr>
	    <th>Nombre</th>
	    <th>Comisi�n</th> 
	    <th>Organizador</th>
	  </tr>
	  </thead>
	  <tbody>
 	 <%
 		QueryResultIterable<Evento> eventos = Evento.loadAll();
	 	for (QueryResultIterator<Evento> eventosI = eventos.iterator(); eventosI.hasNext();) {
			Evento eventoX = (Evento) eventosI.next();			
	 %>
	 <tr>
	    <th><a href='/evento?ID=<%=eventoX.getID()%>'> <%=eventoX.getNombre()%></a></th>
	    <th><%=eventoX.getComisionBoletia()%></th> 
	    <th><%=eventoX.getOrganizador().getNombre()%></th>
	  </tr>	
	<% }  %>
  	</tbody>
	</table>
	 

</body>
</html>
<%@page import="com.googlecode.objectify.Ref"%>
<%@page import="comision.TipoDeBoleto"%>
<%@page import="comision.Evento"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
	<%
		String ID = (String) request.getParameter("ID");
		if(ID == null) return;
		Evento evento = Evento.load(new Long(ID));
	%>
<title><%= evento.getNombre() %></title>

<link href="https://bltassets-cdn3.global.ssl.fastly.net/assets/boletia-ui/boletia-ui-bb2be67448d48811d265b185e446d719.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn0.global.ssl.fastly.net/assets/ep/account-3b7dd201f6a9480151a5a8c137f35d38.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn1.global.ssl.fastly.net/assets/fonts/ss-standard-abae3155fe2e53d096dd2383447b9e60.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn0.global.ssl.fastly.net/assets/fonts/ss-pika-4f41c05ec8287c261b27e4c60f034df2.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn3.global.ssl.fastly.net/assets/css/reveal-41f6888ac8f9af5ae7519f7e1f1e028e.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn3.global.ssl.fastly.net/assets/css/magnific-popup-9c0480fc8a1fc4a39acc49c84110b3cb.css" media="screen" rel="stylesheet" type="text/css" />

</head>
<body class="event-site">

<div class="push-front">
    <script>
var eventVenue = false;
var bookingToken = "-puUqpMBHsyRC0fiejqLqCADMfM";
var maxQuantity = 0;
</script>


<div class="o-event-container">
  <header>
  <section class="o-event-site-header" style="position: relative; background: none;">
    <div class="o-event-site-header__pattern"></div>
      <div class="o-event-site-header__bar no-bg" id="banner-header">
         
  </figure>

    </div>

<!-- event name partial -->

  <div class="anystretch" style="left: 0px; top: 0px; position: absolute; overflow: hidden; z-index: -999998; margin: 0px; padding: 0px; height: 100%; width: 100%;">
  <div style="position: absolute; margin: 0px; padding: 0px; border: none; z-index: -999999; width: 1025.85px; height: 380px; left: -31.9249px; top: 0px;"></div></div></section>
  </header>
  <div class="o-event-site-page clearfix">
    <div class="o-event-site__container clearfix">
      <ul class="o-checkout-steps">
  <li class="selected o-checkout-steps__element">
  	<span class="o-checkout-steps__title">
  		<div class="o-checkout-steps__number">1</div>
  		Boletos
  	</span>
  	<span class="o-checkout-steps__arrow"></span>
  </li>
  <li class=" o-checkout-steps__element">
  	<span class="o-checkout-steps__title o-checkout-steps__number-2">
  		<div class="o-checkout-steps__number">2</div>
  		Forma de pago
  	</span>
  	<span class="o-checkout-steps__arrow"></span>
  </li>
  <li class=" o-checkout-steps__element">
  	<span class="o-checkout-steps__title o-checkout-steps__number-3">
  		<div class="o-checkout-steps__number">3</div>
  		Información adicional
  	</span>
  	<span class="o-checkout-steps__arrow"></span>
  </li>
  <li class=" o-checkout-steps__element">
  	<span class="o-checkout-steps__title">
  		<div class="o-checkout-steps__number">4</div>
  		¡Gracias!
  	</span>
  </li>
</ul>

      <div class="message-block">
	<div class="system-message error" style="display: none;">
	  <h5>Lo sentimos. Revisa los errores e intenta de nuevo:</h5>
	  <ul>
	  </ul>
	</div>
</div>



        <form accept-charset="UTF-8" action="/venta" id="pick-tickets-form" method="post" novalidate="novalidate"><div style="margin:0;padding:0;display:inline"></div>
          <div class="o-event-site-content">
            <div class="c-cart-items__container">

              <table class="o-data-table--cart c-cart-items ">
                <thead>
                  <tr>
                    <th class="c-cart-items__ticket-col text-left">Tipo de boleto</th>
                    <th class="c-cart-items__price-col text-left">Precio</th>
                    <th class="c-cart-items__quantity-col text-right">Cantidad</th>
                  </tr>
                </thead>
                <tbody>
                <%
                	for (Ref<TipoDeBoleto> tBoletoRef : evento.getTipoDeBoletos()) {
        					TipoDeBoleto tBoleto = tBoletoRef.get();
        					if(tBoleto.lugaresDisponibles().size() < 1) continue;
        			 %>
                    <tr class="c-cart-items__ticket-row">
                      <td><h5 class="c-cart-items__title"><%=tBoleto.getNombre()%> </h5></td>
                      <td hidden="" class="c-cart-items__price">1600.0</td>
                      <td>$<%= tBoleto.getPrecio() %> MXN</td>
                      <td class="text-right">
                        <label for="" class="select-wrapper select-wrapper--small num-items-cart">
                          <input name="tipoDeBoleto" type="hidden" value="<%=tBoleto.getID()%>">
                          <select id="ticket_type_<%=tBoleto.getID()%>" name="nBoletos"><option value="0" selected="selected">0</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option></select>
                        </label>
                      </td>
                    </tr>
                    <tr class="c-cart-items__description-row">
                      <td colspan="2"></td>
                      <td></td>
                    </tr>
                    <% } %>
                </tbody>
              </table>
          </div> <!-- emd pf .c-cart-items__container -->
        </div> <!-- end of #main-content -->

        <div class="c-cart-actions">
          <input class="button button-primary button-default boletia-continue-form trk-continue-pay mxp-btn-pick" data-disable-with="Un momento" name="commit" type="submit" value="Continuar">
        </div> <!--end of #checkout -->
        <div class="clearfix"></div>

</form>  </div> <!-- end of #content -->
</div>
</div> <!-- end of #container -->

  </div>
	
</body>
</html>
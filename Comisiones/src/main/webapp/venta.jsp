<%@page import="comision.Reservacion"%>
<%@page import="comision.Boleto"%>
<%@page import="com.googlecode.objectify.Ref"%>
<%@page import="java.util.List"%>
<%@page import="comision.TipoDeBoleto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Venta</title>
 <link href="https://bltassets-cdn3.global.ssl.fastly.net/assets/boletia-ui/boletia-ui-bb2be67448d48811d265b185e446d719.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn0.global.ssl.fastly.net/assets/ep/account-3b7dd201f6a9480151a5a8c137f35d38.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn1.global.ssl.fastly.net/assets/fonts/ss-standard-abae3155fe2e53d096dd2383447b9e60.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn0.global.ssl.fastly.net/assets/fonts/ss-pika-4f41c05ec8287c261b27e4c60f034df2.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn3.global.ssl.fastly.net/assets/css/reveal-41f6888ac8f9af5ae7519f7e1f1e028e.css" media="screen" rel="stylesheet" type="text/css" />
<link href="https://bltassets-cdn3.global.ssl.fastly.net/assets/css/magnific-popup-9c0480fc8a1fc4a39acc49c84110b3cb.css" media="screen" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
function paymentMethodChange(reserv) {
	paymentMethod = document.getElementById("payment_method");
	mPago = paymentMethod.options[paymentMethod.selectedIndex].value;
	data = {
			metodoDePago : mPago,
			reservacion : reserv
		   }
	$.post("/reservacion", data,
			function(data,status){
				if(status == "success"){
					document.getElementById("precioBoletos").innerHTML = "<strong>Subtotal de Boletos:</strong> $" + data.precioBoletos;
				    document.getElementById("comision").innerHTML = "<strong>+ Comisión: </strong> $" + data.comision;
				    document.getElementById("total").innerHTML = " Total: $" + data.total + " MXN";
				}
			 }
			)
}
</script>
</head>
<body>

	<%
		String tipoDeBoletoIDStr = request.getParameter("tipoDeBoleto");
		long tipoDeBoletoID = new Long(tipoDeBoletoIDStr);
		int nBoletos = new Integer(request.getParameter("nBoletos"));
		
		TipoDeBoleto tBoleto = TipoDeBoleto.load(tipoDeBoletoID);
		List<Ref<Boleto>> lugaresDisponibles = tBoleto.lugaresDisponibles();
		if(lugaresDisponibles.size() < nBoletos){
			out.println("Lugares agotados");
			return;
		}
		
		List<Ref<Boleto>> boletos = lugaresDisponibles.subList(0, nBoletos);
		
		Reservacion res = new Reservacion(boletos, tBoleto.getEvento());
		res.setMetodoDePago("defaultPago");
		res.save().now();
	%>
	<div id="container" class="o-event-container clearfix">
  <header>
    <section class="o-event-site-header" style="position: relative; background: none;">
      <div class="o-event-site-header__pattern"></div>
          <div class="o-event-site-header__bar no-bg" id="banner-header">
        
    </div>

<!-- event name partial -->

    <div class="anystretch" style="left: 0px; top: 0px; position: absolute; overflow: hidden; z-index: -999998; margin: 0px; padding: 0px; height: 100%; width: 100%;">
    <div style="position: absolute; margin: 0px; padding: 0px; border: none; z-index: -999999; width: 995px; height: 380px; left: -0.5px; top: 0px;"></div></div></section>
  </header>
  <div class="o-event-site-page clearfix">
    <div class="o-event-site__container clearfix">
      <ul class="o-checkout-steps">
  <li class=" o-checkout-steps__element">
  	<span class="o-checkout-steps__title">
  		<div class="o-checkout-steps__number">1</div>
  		Boletos
  	</span>
  	<span class="o-checkout-steps__arrow"></span>
  </li>
  <li class="selected o-checkout-steps__element">
  	<span class="o-checkout-steps__title o-checkout-steps__number-2">
  		<div class="o-checkout-steps__number">2</div>
  		Forma de pago
  	</span>
  	<span class="o-checkout-steps__arrow"></span>
  </li>
  <li class=" o-checkout-steps__element">
  	<span class="o-checkout-steps__title o-checkout-steps__number-3">
  		<div class="o-checkout-steps__number">3</div>
  		Información adicional
  	</span>
  	<span class="o-checkout-steps__arrow"></span>
  </li>
  <li class=" o-checkout-steps__element">
  	<span class="o-checkout-steps__title">
  		<div class="o-checkout-steps__number">4</div>
  		¡Gracias!
  	</span>
  </li>
</ul>
<div class="o-event-site__container clearfix">

   
<div class="o-event-site-content__main">
        <form accept-charset="UTF-8" action="/comprar" class="o-checkout-form" enctype="multipart/form-data" id="select-payment" method="post" novalidate="novalidate">
        <div style="margin:0;padding:0;display:inline"></div>
          <h1 class="o-checkout-form__title">Reservación</h1>
        
          <div id="js-payment-methods" class="o-checkout-form__payment-methods o-checkout-form__fieldset">
            <h4 class="o-checkout-form__title"> Método de pago</h4>
            <div class="grid-row">
              <div class="col6 form-row">
                <div class="select-wrapper pay-select">
                  <select id="payment_method" name="payment_method" onchange="paymentMethodChange(<%=res.getID() %> )">
                  	<option value="" disabled selected>Por favor selecciona</option>
                  	<option value="deposito">Tiendas Oxxo</option>
					<option value="tarjeta">Tarjeta de crédito o débito</option></select>
                </div>
              </div>
            </div>
          </div> <!-- end of .o-checkout-form__fieldset -->

          <div id="instructions-by-payment-method" data-recording-ignore="mask">
            
          </div>
      </form></div>
	<aside class="o-event-site-sidebar-wrapper scrolling-header-container">
	  <div class="o-event-site-sidebar scrolling-header" style="">
	    <div class="o-cart-summary o-event-site-sidebar__module">
	      <h5 class="o-event-site-sidebar__title"><span class="icon ss-standard ss-cart"></span>  Resumen de compra</h5>
	     
	        <ul class="o-cart-summary__list o-cart-summary__subtotal">
	  <li class="o-cart-summary__item text-right">
	    <div class="grid-row">
	      <div class="col12" id="precioBoletos"><strong>Subtotal de Boletos:</strong> $<%=res.getPrecioBoletos() %> </div>
	    </div>
	  </li>
	
	
	    <li class="o-cart-summary__item text-right">
	      <div class="grid-row">
	        <div class="col12" id="comision"><strong>+ Comisión: </strong> $<%=res.getComisionTotal() %> </div>
	      </div>
	    </li>
	</ul>
	
	      </ul>
	      <div class="order-total o-cart-summary__total text-right" id="total">
	        Total: $<%=res.getVentaTotal() %> MXN
	      </div>
	    </div>
	      <div id="js-order-discount" class="o-order-discount">
	          <a class="o-order-discount__cta" href="#"> ¿Tienes un cupón de descuento?</a>
	          <div id="js-order-discount__form" class="o-order-discount__form">
	            <input id="coupon_code" name="coupon_code" type="text">
	            <div id="js-order-discount__error" class="o-order-discount__error"></div>
	            <a href="#" class="button button-secondary button-small o-order-discount__button" id="js-order-discount__apply">Aplicar cupón</a>
	          </div>
	      </div> <!-- end #discount -->
	  </div>
	</aside>

     <!--  <div class="clearfix"></div>
      <div class="js-scrolling-stop"></div>
      <div class="o-checkout-terms">
        <span>*</span> Al dar click en el botón 'Comprar' estas aceptando nuestros términos. <a href="#" data-reveal-my-id="js-terms-of-service"><span class="icon ss-icon ss-info"></span> Revisa los términos de compra.</a>
      </div>

      <div class="c-cart-actions">
        <a href="/bookings/bdb3650daf/pick_tickets" class="button button-link inline-block-middle">Regresar al paso anterior</a> o
        <input class="button button-primary button-default fb-trk-buy-or-book mxp-btn-pay" data-disable-with="Un momento" id="buy_or_book_button" name="commit" type="submit" value="Comprar">
      </div>
      <div class="clearfix"></div>
    </div><!-- end of .o-event-site__container -->
  </div>
</div>
	
</body>
</html>
/**
 * 
 */
package comision;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.Work;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

/**
 * @author Pablo
 *
 */
@Entity
public class Reservacion {
	
	public Long getID() {
		return ID;
	}

	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Reservacion.class.getName());
	
	private @Id Long ID; 
	private @Load List<Ref<Boleto>> boletos;
	private @Index double precioBoletos;
	private @Index double comisionTotal;
	private @Index double ventaTotal;
	private Comision comisionMetodoDePago;
	private @Index Evento evento;
	
	
	public Reservacion() {}

	/**
	 * @param boletos
	 * @param metodoDePago
	 * @param evento
	 */
	public Reservacion(List<Ref<Boleto>> boletos, Evento evento) {
		this.boletos = boletos;
		this.evento = evento;
	}

	public Result<Key<Reservacion>> save() {
		return ofy().save().entity(this);
	}
	
	public static Reservacion load(long ID) {
		return ofy().load().type(Reservacion.class).id(ID).now();
	}
	
	private double calcularPrecioBoletos() {
		precioBoletos = 0;
		for (Ref<Boleto> boleto : boletos) {
			precioBoletos += boleto.get().getPrecio();
		}
		return precioBoletos;
	}
	
	private double calcularComisionTotal() {
		Comision comisionBoletia = evento.getComisionBoletia();
		
		comisionTotal = 0;
		
		for (Ref<Boleto> boleto : boletos) {
			double precio = boleto.get().getPrecio();
			comisionTotal += comisionMetodoDePago.calcularComision(precio) 
					+ comisionBoletia.calcularComision(precio);
		}
		
		return comisionTotal;
	}
	
	private void calcularTotal() {
		this.ventaTotal = this.calcularPrecioBoletos() + this.calcularComisionTotal();
	}
	
	/**
	 * Intenta vender cada boleto pero si uno no puede ser vendido ninguno es vendido
	 * @return "Exito" / "Error: " + e.getMessage()
	 */
	public String vender() {
		return ofy().transact(new Work<String>() {

			@Override
			public String run() {
				try {
					for (Ref<Boleto> boletoRef : boletos) {
						Boleto boleto = boletoRef.get();
						boleto.vender();
						boleto.save();
					}
					return "Exito";
				} catch (Exception e) {
					return "Error: " + e.getMessage();
				}
			}

			
		});
	}
	
	public void setMetodoDePago(String metodoDePagoStr) {
		this.comisionMetodoDePago = Comision.load(metodoDePagoStr);
		this.calcularTotal();
	}

	public double getPrecioBoletos() {
		return precioBoletos;
	}

	public double getComisionTotal() {
		return comisionTotal;
	}

	public double getVentaTotal() {
		return ventaTotal;
	}
	
	

}

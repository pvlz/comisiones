/**
 * 
 */
package comision;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.VoidWork;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

/**
 * @author Pablo
 *
 */
@Entity
public class TipoDeBoleto {
	
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(TipoDeBoleto.class.getName());
	
	private @Id Long ID; 
	private @Index String nombre;
	private int numeroBoletos;
	private double precio;
	private @Index Ref<Evento> evento;
	private @Load List<Ref<Boleto>> boletos;
	
	public TipoDeBoleto() {}
	
	public TipoDeBoleto(String nombre, int numeroBoletos, double precio, Evento evento) {
		this.nombre = nombre;
		this.numeroBoletos = numeroBoletos;
		this.precio = precio;
		if(evento.getID() == null) {
			evento.save().now();
		}
		this.evento = Ref.create(evento);		
	}
	
	public Result<Key<TipoDeBoleto>> save() {
		return ofy().save().entity(this);
	}
	
	public static TipoDeBoleto load(long ID) {
		return ofy().load().type(TipoDeBoleto.class).id(ID).now();
	}
	
	public List<Ref<Boleto>> lugaresDisponibles(){
		List<Ref<Boleto>> disponibles = new ArrayList<>();
		
		for(Ref<Boleto> boletoRef : this.getBoletos()) {
			Boleto boleto = boletoRef.get();
			if(boleto.getStatus() == Boleto.DISPONIBLE) {
				disponibles.add(boletoRef);
			}
		}
		return disponibles;
	}

	private List<Ref<Boleto>> getBoletos() {
		if(this.boletos == null) {
			this.boletos = new ArrayList<>();
			ofy().transact(new VoidWork() {
				@Override
				public void vrun() {
					for (long i = 1; i <= numeroBoletos; i++) {
						Boleto boleto = new Boleto(i, TipoDeBoleto.this, TipoDeBoleto.this.evento);
						boleto.save();
						TipoDeBoleto.this.boletos.add(Ref.create(boleto));
						if(i%50 == 0) {
							ofy().clear();
						}
					}
					TipoDeBoleto.this.save();
				}
			});
		}		
		return this.boletos;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public Long getID() {
		return ID;
	}

	public Evento getEvento() {
		return evento.get();
	}

}

/**
 * 
 */
package comision;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Pablo
 *
 */
@Entity
public class Comision {

	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Comision.class.getName());
	
	private @Id Long ID; 
	private @Index String nombre;
	private double multiplicador;
	private double agregador;
	
	public Comision() {}
	
	public Comision(double multiplicador, double agregador) {
		this.multiplicador = multiplicador;
		this.agregador = agregador;
	}
	
	public Comision(String nombre, double multiplicador, double agregador) {
		this(multiplicador, agregador);
		this.nombre = nombre;
	}

	public Comision(String multiplicador, String agregador) {
		this(new Double(multiplicador), new Double(agregador));
	}

	public Result<Key<Comision>> save() {
		return ofy().save().entity(this);
	}
	
	public static Comision load(long ID) {
		return ofy().load().type(Comision.class).id(ID).now();
	}
	
	public static Comision load(String nombre) {
		return ofy().load().type(Comision.class).filter("nombre", nombre).first().now();
	}

	public double calcularComision(double precio) {
		return precio * this.multiplicador + this.agregador;
	}
	
	public String toString() {
		return this.multiplicador * 100 + "% + " + agregador;
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

}

/**
 * 
 */
package comision;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

/**
 * @author Pablo
 *
 */
@Entity
public class Organizador {
	
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Organizador.class.getName());
	
	private @Id Long ID; 
	private @Index String nombre;
	private @Load List<Ref<Evento>> eventos;
	private Ref<Comision> comisionBoletia;
	
	/**
	 * Constructor requerido para la base de datos
	 */
	public Organizador() {}
	
	/**
	 * 
	 * @param nombre
	 * @param comisionBoletia
	 */
	public Organizador(String nombre, Comision comisionBoletia) {
		this.nombre = nombre; //TODO no permitir nombres existentes en DB
		if(comisionBoletia == null) {
			this.setComisionBoletia(Comision.load("defaultBoletia"));
		}else {
			this.setComisionBoletia(comisionBoletia);
		}
	}
	
	public Result<Key<Organizador>> save() {
		return ofy().save().entity(this);
	}
	
	public static Organizador load(long ID) {
		return ofy().load().type(Organizador.class).id(ID).now();
	}
	
	public void asignarEvento(Evento evento) {
		this.getEventos().add(Ref.create(evento));
	}
	
	private List<Ref<Evento>> getEventos() {
		if(this.eventos == null) {
			this.eventos = new ArrayList<>();
		}
		return this.eventos;
	}

	public Comision getComisionBoletia() {
		return comisionBoletia.get();
	}

	public void setComisionBoletia(Comision comisionBoletia) {
		if(comisionBoletia.getID() == null) comisionBoletia.save().now();
		this.comisionBoletia = Ref.create(comisionBoletia);
	}

	public Long getID() {
		return this.ID;
	}

	public String getNombre() {
		return nombre;
	}
}

/**
 * 
 */
package comision;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.logging.Logger;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

/**
 * @author Pablo
 *
 */
@Entity
public class Boleto {
	
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Boleto.class.getName());

	public static final int DISPONIBLE = 1;
	public static final int APARTADO = 2;
	public static final int VENDIDO = 3;
	
	private @Id Long ID; 
	private @Index int status;
	private @Index Ref<TipoDeBoleto> tipoDeBoleto;
	private @Parent Ref<Evento> evento;
	private @Index Reservacion reservacion;
	
	
	public Boleto() {}


	/**
	 * @param tipoDeBoleto
	 * @param evento
	 */
	public Boleto(Long ID, TipoDeBoleto tipoDeBoleto, Ref<Evento> evento) {
		this.ID = ID;
		this.tipoDeBoleto = Ref.create(tipoDeBoleto);
		this.evento = evento;
		this.status = DISPONIBLE;
	}
	
	public Result<Key<Boleto>> save() {
		return ofy().save().entity(this);
	}
	
	public static Boleto load(long ID) {
		return ofy().load().type(Boleto.class).id(ID).now();
	}
	
	public void apartar() throws Exception {
		if(this.status == DISPONIBLE) {
			this.status = APARTADO;
		}else {
			throw new Exception("Boleto no disponible"); //TODO crear NoDisponibleException
		}
	}
	
	public void vender() throws Exception {
		if(this.status == DISPONIBLE || this.status == APARTADO) {
			this.status = VENDIDO;
		}else {
			throw new Exception("Boleto no disponible"); //TODO crear NoDisponibleException
		}
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public double getPrecio() {
		return this.tipoDeBoleto.get().getPrecio();
	}


	
	
}

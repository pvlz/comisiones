/**
 * 
 */
package comision;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Result;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

/**
 * @author Pablo
 *
 */
@Entity
public class Evento {
	
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(Evento.class.getName());
	
	private @Id Long ID; 
	private @Index String nombre;
	private @Load List<Ref<TipoDeBoleto>> tipoDeBoletos;
	private Ref<Comision> comisionBoletia;
	private @Index Ref<Organizador> organizador;
	
	public Evento() {}
	
	public Evento(String nombre, Organizador organizador, Comision comision) {
		this.nombre = nombre;
		if(organizador.getID() == null) {
			organizador.save().now();
		}
		this.organizador = Ref.create(organizador);
		if(comision == null) {
			this.setComisionBoletia(organizador.getComisionBoletia());
		}else {
			this.setComisionBoletia(comision);
		}
	}
	
	public Result<Key<Evento>> save() {
		return ofy().save().entity(this);
	}
	
	public static Evento load(long ID) {
		return ofy().load().type(Evento.class).id(ID).now();
	}
	
	public static QueryResultIterable<Evento> loadAll() {
		return ofy().load().type(Evento.class).iterable();
	}
	
	public void asignarTipoDeBoleto(TipoDeBoleto tipoDeBoleto) {
		this.getTipoDeBoletos().add(Ref.create(tipoDeBoleto));
	}
	
	public List<Ref<TipoDeBoleto>> getTipoDeBoletos() {
		if(this.tipoDeBoletos == null) {
			this.tipoDeBoletos = new ArrayList<>();
		}
		return this.tipoDeBoletos;
	}

	public Comision getComisionBoletia() {
		return comisionBoletia.get();
	}

	public void setComisionBoletia(Comision comisionBoletia) {
		if(comisionBoletia.getID() == null) comisionBoletia.save().now();
		this.comisionBoletia = Ref.create(comisionBoletia);
	}

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getNombre() {
		return nombre;
	}

	public Organizador getOrganizador() {
		return organizador.get();
	}
}

/**
 * 
 */
package implementacion;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;

import comision.Boleto;
import comision.Comision;
import comision.Evento;
import comision.Organizador;
import comision.Reservacion;
import comision.TipoDeBoleto;

/**
 * @author Pablo
 * Este servlet prepara la base de datos
 */
@WebServlet(
	    name = "Arranque",
	   // loadOnStartup = 1,
	    urlPatterns = {"/arranque"}
	)
public class Arranque extends HttpServlet {
	private static final long serialVersionUID = 3909163683826037927L;

	static {
		 registrarClases();
		 }

	public static void registrarClases() {
		 ObjectifyService.register(Organizador.class);
		 ObjectifyService.register(Evento.class);
		 ObjectifyService.register(TipoDeBoleto.class);
		 ObjectifyService.register(Boleto.class);
		 ObjectifyService.register(Reservacion.class);
		 ObjectifyService.register(Comision.class);
	}

	private static String CONTEXT_PATH;
	
	@Override
	  public void doGet(HttpServletRequest request, HttpServletResponse response) 
	      throws IOException {
	      
	    response.setContentType("text/plain");
	    response.setCharacterEncoding("UTF-8");

	    response.getWriter().print("Entidades registradas");

	  }
	
	public void init(){
		ServletContext context = getServletContext();
		CONTEXT_PATH = context.getRealPath("/") + "/";
	}

	public static String getCONTEXT_PATH() {
		if(CONTEXT_PATH == null){
			try {
				CONTEXT_PATH = new File(".").getCanonicalPath() + "\\src\\main\\webapp\\";
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return CONTEXT_PATH;
	}

}

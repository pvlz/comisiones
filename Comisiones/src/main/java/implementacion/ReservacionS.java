package implementacion;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.mortbay.util.ajax.JSON;

import comision.Reservacion;

/**
 * Servlet implementation class ReservacionS
 */
@WebServlet("/reservacion")
public class ReservacionS extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * 
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String metodoDePagoStr = request.getParameter("metodoDePago");
		String reservacionIDStr = request.getParameter("reservacion");
		
		Long resrvacionID = new Long(reservacionIDStr);
		Reservacion reservacion = Reservacion.load(resrvacionID);
		reservacion.setMetodoDePago(metodoDePagoStr);
		reservacion.save();
		
		HashMap<String, Double> res = new HashMap<>();
		res.put("precioBoletos", reservacion.getPrecioBoletos());
		res.put("comision", reservacion.getComisionTotal());
		res.put("total", reservacion.getVentaTotal());
		
		response.setContentType("application/json");
		response.getWriter().println(JSON.toString(res));
	}

}

/**
 * 
 */
package implementacion;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.VoidWork;

import comision.Comision;
import comision.Evento;
import comision.Organizador;
import comision.TipoDeBoleto;

/**
 * @author Pablo
 *
 */
@WebServlet(
	    name = "Adminorganizadores",
	    urlPatterns = {"/adminorganizadores"}
	)
public class AdminOrganizadores extends HttpServlet {
	private static final long serialVersionUID = 3780581251892535839L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) 
		      throws IOException {
		
	}
	
	@Override
	  public void doPost(HttpServletRequest request, HttpServletResponse response) 
	      throws IOException {
		
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
	    	    	    
	    try {
			String nombreOrganizador = request.getParameter("nombreOrganizador");
			String porcentajeOrganizador = request.getParameter("porcentajeOrganizador");
			String sumaOrganizador = request.getParameter("sumaOrganizador");
			String nombreEvento = request.getParameter("nombreEvento");
			String porcentajeEvento = request.getParameter("porcentajeEvento");
			String sumaEvento = request.getParameter("sumaEvento");
			String nombreTipoDeBoleto = request.getParameter("nombreTipoDeBoleto");
			int numBoletos = new Integer(request.getParameter("numBoletos"));
			double precio = new Double(request.getParameter("precio"));
			
			Comision cOrganizador = null;
			if(!porcentajeOrganizador.isEmpty() && !sumaOrganizador.isEmpty()) {
				cOrganizador = new Comision(porcentajeOrganizador, sumaOrganizador);
			}
			
			Organizador org = new Organizador(nombreOrganizador, cOrganizador);
			
			Comision cEvento = null;
			if(!porcentajeEvento.isEmpty() && !sumaEvento.isEmpty()) {
				cEvento = new Comision(porcentajeEvento, sumaEvento);
			}
			Evento eve = new Evento(nombreEvento, org, cEvento);
			TipoDeBoleto tBoleto = new TipoDeBoleto(nombreTipoDeBoleto, numBoletos, precio, eve);
			
			
			ofy().transact(new VoidWork() {
				@Override
				public void vrun() {
					tBoleto.save().now();
					eve.asignarTipoDeBoleto(tBoleto);
					eve.save().now();
					org.asignarEvento(eve);
					org.save();
				}
			});
			
			response.sendRedirect("eventos.jsp");
		} catch (NumberFormatException e) {
			e.printStackTrace();
			response.sendError(1, e.getMessage());
		}
	  }

}

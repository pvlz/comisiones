/**
 * 
 */
package comision;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.VoidWork;

import implementacion.Arranque;

/**
 * @author Pablo
 *
 */
public class PruebaTecnica1 {
	
	private final LocalServiceTestHelper helper =
		      new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		helper.setUp();
		ObjectifyService.run(new VoidWork() {			
			@Override
			public void vrun() {
				Arranque.registrarClases();
				//previamente se tienen las comisiones de metodo de pago
				new Comision("defaultBoletia", .2, 0).save().now();
				
				new Comision("defaultPago", 0, 20).save().now();
				new Comision("tarjeta", 0.035, 0).save().now();
				new Comision("deposito", 0, 10).save().now();
			}
		});
	}
	
	 @After
	  public void tearDown() {
	    helper.tearDown();
	  }
	
	/**
	 * Dado que se tienen comisiones por default por método de pago, Cuando
	 * se tiene 1 boleto X con valor 500, Entonces calcular el costo final de
	 * la reservación.
	 */
	@Test
	public final void test1() {		
		ObjectifyService.run(new VoidWork() {		
			@Override
			public void vrun() {				
				//el organizador registra su evento
				Organizador org = new Organizador("Juan", null);
				Evento eve = new Evento("Tchaikovsky", org, null);
				TipoDeBoleto tBoleto = new TipoDeBoleto("VIP", 30, 500, eve);
				
				//lo guarda
				tBoleto.save().now();
				eve.asignarTipoDeBoleto(tBoleto);
				eve.save().now();
				org.asignarEvento(eve);
				org.save();
				
				//el cliente compra un boleto
				List<Ref<Boleto>> boletos = eve.getTipoDeBoletos().get(0).get().lugaresDisponibles().subList(3, 4);
				Reservacion res = new Reservacion(boletos, eve);
				res.setMetodoDePago("defaultPago");
				
				assertEquals(500, res.getPrecioBoletos(), 0);
				assertEquals(120, res.getComisionTotal(), 0);
				assertEquals(620, res.getVentaTotal(), 0);
			}
		});
	}
	
	/**
	 * Dado que se tienen comisiones por default por método de pago,
	 * Cuando se tiene un boleto X con valor 500,
	 * Y se quieren personalizar las comisiones por organizador,
	 * Entonces calcular el costo final de la reservación en base a las
	 * nuevas comisiones por organizador.
	 */
	@Test
	public final void test2() {		
		ObjectifyService.run(new VoidWork() {		
			@Override
			public void vrun() {				
				//el organizador registra su evento
				Organizador org = new Organizador("Juan", new Comision(.1, 0));
				Evento eve = new Evento("Tchaikovsky", org, null);
				TipoDeBoleto tBoleto = new TipoDeBoleto("VIP", 30, 500, eve);
				
				//lo guarda
				tBoleto.save().now();
				eve.asignarTipoDeBoleto(tBoleto);
				eve.save().now();
				org.asignarEvento(eve);
				org.save();
				
				//el cliente compra un boleto
				List<Ref<Boleto>> boletos = eve.getTipoDeBoletos().get(0).get().lugaresDisponibles().subList(3, 4);
				Reservacion res = new Reservacion(boletos, eve);
				res.setMetodoDePago("defaultPago");
				
				assertEquals(500, res.getPrecioBoletos(), 0);
				assertEquals(70, res.getComisionTotal(), 0);
				assertEquals(570, res.getVentaTotal(), 0);
			}
		});
	}
	
	/**
	 * Dado que se tienen comisiones por default por método de pago,
	 * Cuando se tiene un boleto X con valor 500,
	 * Y se quieren personalizar las comisiones por evento,
	 * Entonces calcular el costo final de la reservación en base a las	
	 * nuevas comisiones por evento.
	 */
	@Test
	public final void test3() {		
		ObjectifyService.run(new VoidWork() {		
			@Override
			public void vrun() {				
				//el organizador registra su evento
				Organizador org = new Organizador("Juan", null);
				Evento eve = new Evento("Tchaikovsky", org, new Comision(0, 1));
				TipoDeBoleto tBoleto = new TipoDeBoleto("VIP", 30, 500, eve);
				
				//lo guarda
				tBoleto.save().now();
				eve.asignarTipoDeBoleto(tBoleto);
				eve.save().now();
				org.asignarEvento(eve);
				org.save();
				
				//el cliente compra un boleto
				List<Ref<Boleto>> boletos = eve.getTipoDeBoletos().get(0).get().lugaresDisponibles().subList(3, 4);
				Reservacion res = new Reservacion(boletos, eve);
				res.setMetodoDePago("defaultPago");
				
				assertEquals(500, res.getPrecioBoletos(), 0);
				assertEquals(21, res.getComisionTotal(), 0);
				assertEquals(521, res.getVentaTotal(), 0);
			}
		});
	}
	
	/**
	 * Dado que se tienen comisiones por organizador
	 * Crear en base a éstas, comisiones por evento,
	 * Entonces calcular el costo final de la reservación en base a las
	 * nuevas comisiones por evento.
	 */
	@Test
	public final void test4() {		
		ObjectifyService.run(new VoidWork() {		
			@Override
			public void vrun() {				
				//el organizador registra su evento
				Organizador org = new Organizador("Juan",  new Comision(0, 999));
				Evento eve = new Evento("Tchaikovsky", org, new Comision(0, 1));
				TipoDeBoleto tBoleto = new TipoDeBoleto("VIP", 30, 500, eve);
				
				//lo guarda
				tBoleto.save().now();
				eve.asignarTipoDeBoleto(tBoleto);
				eve.save().now();
				org.asignarEvento(eve);
				org.save();
				
				//el cliente compra un boleto
				List<Ref<Boleto>> boletos = eve.getTipoDeBoletos().get(0).get().lugaresDisponibles().subList(3, 4);
				Reservacion res = new Reservacion(boletos, eve);
				res.setMetodoDePago("tarjeta");
				
				assertEquals(500, res.getPrecioBoletos(), 0);
				assertEquals(18.5, res.getComisionTotal(), 0);
				assertEquals(518.5, res.getVentaTotal(), 0);
			}
		});
	}
	
	/**
	 * Dado que un organizador crea una reservación,
	 * Cuando selecciona un método de pago,
	 * Entonces calcular el costo final de la reservación, considerando la
	 * comisiones ya sea por evento o por organizador.
	 */
	@Test
	public final void test5organizador() {		
		ObjectifyService.run(new VoidWork() {		
			@Override
			public void vrun() {				
				//el organizador registra su evento
				Organizador org = new Organizador("Juan",  new Comision(0, 90));
				Evento eve = new Evento("Tchaikovsky", org, null);
				TipoDeBoleto tBoleto = new TipoDeBoleto("VIP", 30, 500, eve);
				
				//lo guarda
				tBoleto.save().now();
				eve.asignarTipoDeBoleto(tBoleto);
				eve.save().now();
				org.asignarEvento(eve);
				org.save();
				
				//el cliente compra un boleto
				List<Ref<Boleto>> boletos = eve.getTipoDeBoletos().get(0).get().lugaresDisponibles().subList(3, 6);
				Reservacion res = new Reservacion(boletos, eve);
				res.setMetodoDePago("deposito");
				
				assertEquals(1500, res.getPrecioBoletos(), 0);
				assertEquals(300, res.getComisionTotal(), 0);
				assertEquals(1800, res.getVentaTotal(), 0);
			}
		});
	}
	
	/**
	 * Dado que un organizador crea una reservación,
	 * Cuando selecciona un método de pago,
	 * Entonces calcular el costo final de la reservación, considerando la
	 * comisiones ya sea por evento o por organizador.
	 */
	@Test
	public final void test5evento() {		
		ObjectifyService.run(new VoidWork() {		
			@Override
			public void vrun() {				
				//el organizador registra su evento
				Organizador org = new Organizador("Juan", null);
				Evento eve = new Evento("Tchaikovsky", org,  new Comision(.1, 0));
				TipoDeBoleto tBoleto = new TipoDeBoleto("VIP", 30, 500, eve);
				
				//lo guarda
				tBoleto.save().now();
				eve.asignarTipoDeBoleto(tBoleto);
				eve.save().now();
				org.asignarEvento(eve);
				org.save();
				
				//el cliente compra un boleto
				List<Ref<Boleto>> boletos = eve.getTipoDeBoletos().get(0).get().lugaresDisponibles().subList(3, 9);
				Reservacion res = new Reservacion(boletos, eve);
				res.setMetodoDePago("deposito");
				
				assertEquals(3000, res.getPrecioBoletos(), 0);
				assertEquals(360, res.getComisionTotal(), 0);
				assertEquals(3360, res.getVentaTotal(), 0);
			}
		});
	}
}

# Comisiones #

Calcula el precio con comisiones personalizadas seg�n el evento u organizador, as� como comisiones personalizadas por metodo de pago.

### Modulos ###

* [Comisiones](https://bitbucket.org/pvlz/comisiones/src/80325950f684545478d3fec5487e621c6b7a8273/Comisiones/src/main/java/comision/?at=master)
> [Testing](https://bitbucket.org/pvlz/comisiones/src/80325950f684545478d3fec5487e621c6b7a8273/Comisiones/src/test/java/comision/?at=master)
* [Implementacion](https://bitbucket.org/pvlz/comisiones/src/80325950f684545478d3fec5487e621c6b7a8273/Comisiones/src/main/java/implementacion/?at=master)
> [Testing](https://bitbucket.org/pvlz/comisiones/src/80325950f684545478d3fec5487e621c6b7a8273/Comisiones/src/test/java/implementacion/?at=master)

### Detalles del proyecto ###

* Lenguaje JAVA
* DB Google datastore

* Inclu� la configuraci�n de Eclipse por lo que solo se requiere importar el proyecto y descargar el plugin de Google Cloud